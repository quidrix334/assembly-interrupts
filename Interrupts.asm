#include "p16f877.inc"

		counter equ 20h
		org 0
		goto main
		org 004h

inc:	incf counter
		bcf INTCON,INTF
		retfie

set_timer:
		movlw d'61'
		movwf TMR0
		bcf INTCON,T0IF
		return

display:
		movf counter,w
		movwf PORTB
		return

set_registers:
		bsf STATUS,RP0
		clrf TRISB
		movlw b'11010111' ;/256
		movwf OPTION_REG
		bcf STATUS,RP0
		bcf INTCON,INTF
		bsf INTCON,INTE
		bsf INTCON,GIE
		return

main	clrf counter
		call set_registers
restart	call set_timer
loop	btfss INTCON,T0IF
		goto loop
		call inc
		call display
		goto restart
		end